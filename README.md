# Instalacion del Facturador PROv5 por Victor Candela


## Comencemos

Este repositorio les servira como alternativa para instalar el Facturador PROv5 con docker-compose independiente de la Version del FactuPRO

## Descargar Repositorio

```
git clone git@gitlab.com:vcandela/docker-factuprov5.git FactuPROv5
```

## Opcional - Descargar Imagen terminada de Docker Hub
```
docker pull vcandela/factuprov5_ubuntu20:php74
```

Luego ejecutar los siguientes comandos
```
cd FactuPROv5
git clone git@gitlab.com:facturaloperu/facturador/pro5.git core
docker-compose pull
docker-compose up -d
```

Cambiar la Version del php del composer.json del FactuPROv5 en la carpeta core
```
"php": "^7.4",
```


## Continuar con la Instalacion de las dependencias del sistema
```
docker exec -it FactuPROv5 bash
composer install
cp .env.example .env
php artisan key:generate
php artisan storage:link
chmod 777 -R storage bootstrap public
```

## Configurar el .env con los Siguientes Datos:
```
DB_CONNECTION=mysql
DB_HOST=docker_db
DB_PORT=3306
DB_DATABASE=tenancy
DB_USERNAME=root
DB_PASSWORD=FactuPROv5_2022
```

## Instalar los datos de la BD Inicial:
```
php artisan migrate --seed
```

## Entrar por el Link que tenga su Host con el Puerto que configure en el docker-compose.yml
